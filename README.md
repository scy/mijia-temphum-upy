# mijia-temphum-upy

This is MicroPython code to read temperature and humidity from certain Xiaomi Mijia sensors.

**Note:**
Consider this beta quality software, I'm in a hurry.

## Requirements

This requires MicroPython, a board with BLE support and the [Perthensis](https://codeberg.org/scy/perthensis) `gattc` module.

The latter is currently the most complicated requirement:
`gattc` is still in development and not yet available in a stable branch.

## Example

### A single sensor

```python
from mijia_temphum import Central, Sensor

def result(val):
    print("{0} is at {1}°C and {2}%".format(
        val.sensor.hexaddr, val.temp_c, val.hum_pcnt))

central = Central()
central.enable()

s = Sensor(central, "4C:65:A8:DC:1B:6A", result)
s.request_value()
```

### Multiple sensors

```python
from mijia_temphum import Central, Sensors

def result(val):
    print("{0} ({1}) is at {2}°C and {3}%".format(
        val.sensor.name, val.sensor.hexaddr, val.temp_c, val.hum_pcnt))

def status(done, of, elapsed_s):
    print("{0} of {1} sensors read after {2}s".format(
        done, of, elapsed_s))

central = Central()
central.enable()

s = Sensors(central, {
    "4C:65:A8:DC:1B:6A": "Desk",
    "4C:65:A8:DC:29:07": "Kitchen",
}, result, status)
s.request_all()
```

### Multiple sensors regularly

This is using the coroutine scheduler provided by Perthensis.

```python
from mijia_temphum import Central, ScheduledSensors
from perthensis.scheduler import Scheduler

def result(val):
    print("{0} ({1}) is at {2}°C and {3}%".format(
        val.sensor.name, val.sensor.hexaddr, val.temp_c, val.hum_pcnt))

def status(done, of, elapsed_s):
    print("{0} of {1} sensors read after {2}s".format(
        done, of, elapsed_s))

central = Central()
central.enable()

s = ScheduledSensors(central, {
    "4C:65:A8:DC:1B:6A": "Desk",
    "4C:65:A8:DC:29:07": "Kitchen",
}, result, status, 300)  # last number is interval in seconds

sch = Scheduler()
sch(s.schedule)
sch.run_forever()
```
